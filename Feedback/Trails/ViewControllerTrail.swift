//
//  ViewControllerTrail.swift
//  Feedback
//
//  Created by Mohamed Afsar on 19/02/21.
//

import FeedbackKit

internal enum ViewControllerTrail: String {
    case infoTapped
    case successTapped
    case failureTapped
    case warningTapped
}

extension ViewControllerTrail: Trailable {
    var eventName: String { self.rawValue }
    
    var type: TrailType {
        switch self {
        case .infoTapped: return .info
        case .successTapped: return .success
        case .failureTapped: return .failure
        case .warningTapped: return .warning
        }
    }
    
    var logParams: [String : Any?]? {
        switch self {
        case .infoTapped:
            return ["1": "One", "2": "Two", "3": "Three"] // NO I18N
        case .successTapped:
            return ["1": "One", "2": "Two", "3": "Three"] // NO I18N
        case .failureTapped:
            return ["1": "One", "2": "Two", "3": "Three"] // NO I18N
        case .warningTapped:
            return ["1": "One", "2": "Two", "3": "Three"] // NO I18N
        }
    }
    
    var trackEventName: String? {
        return nil
    }
    
    var trackParams: [String : Any?]? {
        return nil
    }
    
    var trackGroupName: String? {
        return nil
    }
}
