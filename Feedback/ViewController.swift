//
//  ViewController.swift
//  Feedback
//
//  Created by Mohamed Afsar on 18/02/21.
//

import UIKit
import FeedbackKit

class ViewController: UIViewController {
    // MARK: Private Static ICons
    private static let _trail = true
    
    // MARK: Private ICons
    private let _trailer: Trailer = {
        let tracker = TrackerBuilder().build()
        let fileLogger = FileLoggerBuilder().build()
        let consoleLogger = ConsoleLoggerBuilder().build()
        
        return TrailerBuilder()
        .with(tracker: tracker)
        .with(fileLogger: fileLogger)
        .with(consoleLogger: consoleLogger)
        .build()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Feedback" // NO I18N
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = false
        _setup()
        _addViews()
    }
}

// MARK: Helper Functions
private extension ViewController {
    func _setup() {
        NotificationCenter.default.addObserver(forName: .fk_didSendFeedback, object: nil, queue: .main) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let successVC = SuccessViewController()
                self.present(successVC, animated: false, completion: nil)
            }
        }
    }
    
    func _addViews() {
        let stackVw = UIStackView()
        stackVw.axis = .vertical
        stackVw.translatesAutoresizingMaskIntoConstraints = false
        stackVw.distribution = .fillEqually
        stackVw.spacing = 5
        
        let actions = Actions.allCases
        for i in 0..<actions.count {
            let action = actions[i]
            let btn = UIButton(type: .roundedRect)
            btn.setTitle(action.title(), for: .normal)
            btn.setTitleColor(action.titleColor(), for: .normal)
            btn.titleLabel?.font = action.font()
            btn.layer.borderWidth = 1 / UIScreen.main.scale
            btn.layer.cornerRadius = 6
            btn.layer.borderColor = action.borderColor().cgColor
            btn.tag = i
            btn.addTarget(self, action: #selector(_actionBtnTapped(_:)), for: .touchUpInside)
            stackVw.addArrangedSubview(btn)
        }
        
        view.addSubview(stackVw)
        NSLayoutConstraint.activate([
            stackVw.leadingAnchor.constraint(equalTo: stackVw.superview!.leadingAnchor, constant: 15),
            stackVw.topAnchor.constraint(equalTo: stackVw.superview!.topAnchor),
            stackVw.trailingAnchor.constraint(equalTo: stackVw.superview!.trailingAnchor, constant: -15),
            stackVw.bottomAnchor.constraint(equalTo: stackVw.superview!.bottomAnchor, constant: -15),
        ])
    }
    
    func _animate(emoji: Character) {
        let emojiLbl = _add(emoji: emoji)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            let btmConstraints = emojiLbl.superview?.constraints.filter({ $0.firstItem === emojiLbl && $0.firstAttribute == NSLayoutConstraint.Attribute.top})
            btmConstraints?.first?.isActive = false
                        
            emojiLbl.bottomAnchor.constraint(equalTo: emojiLbl.superview!.topAnchor, constant: -100).isActive = true
            
            let animationOptions: UIView.AnimationOptions = .curveEaseOut
            let keyframeAnimationOptions: UIView.KeyframeAnimationOptions = UIView.KeyframeAnimationOptions(rawValue: animationOptions.rawValue)
            
            UIView.animateKeyframes(withDuration: 1, delay: 0, options: [.allowUserInteraction, keyframeAnimationOptions], animations: {
                
                emojiLbl.superview!.layoutIfNeeded()
                emojiLbl.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
                emojiLbl.alpha = 0
                
            }, completion: { _ in
                emojiLbl.removeFromSuperview()
            })
        }
    }
    
    func _add(emoji: Character) -> UILabel {
        let label = UILabel()
        label.text = String(emoji)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.systemFont(ofSize: 300)
        label.minimumScaleFactor = 0.1
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(_emojiLblTapped(_:))))
        
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.widthAnchor.constraint(equalTo: label.superview!.widthAnchor),
            label.heightAnchor.constraint(equalTo: label.widthAnchor),
            label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor),
            label.topAnchor.constraint(equalTo: label.superview!.bottomAnchor)
        ])
        return label
    }
}

// MARK: Action Functions
private extension ViewController {
    @objc func _actionBtnTapped(_ sender: UIButton) {
        let action = Actions.allCases[sender.tag]
        switch action {
        case .info:
            _trailer.convenienceTrail(ViewControllerTrail.infoTapped, trail: ViewController._trail)
        case .success:
            _trailer.convenienceTrail(ViewControllerTrail.successTapped, trail: ViewController._trail)
        case .failure:
            _trailer.convenienceTrail(ViewControllerTrail.failureTapped, trail: ViewController._trail)
        case .warning:
            _trailer.convenienceTrail(ViewControllerTrail.warningTapped, trail: ViewController._trail)
        }
        self._animate(emoji: action.indicator())
    }
    
    @objc func _emojiLblTapped(_ gestureRecognizer: UITapGestureRecognizer) {
        gestureRecognizer.view?.removeFromSuperview()
    }
}

private enum Actions: CaseIterable {
    case info
    case success
    case warning
    case failure
    
    func title() -> String {
        switch self {
        case .info: return "Info 🙂" // NO I18N
        case .success: return "Success 😎" // NO I18N
        case .warning: return "Warning 🧐" // NO I18N
        case .failure: return "Failure 😱" // NO I18N
        }
    }
    
    func titleColor() -> UIColor {
        return .white
    }
    
    func font() -> UIFont {
        return UIFont.systemFont(ofSize: 25)
    }
    
    func borderColor() -> UIColor {
        switch self {
        case .info: return UIColor.blue.withAlphaComponent(0.6)
        case .success: return UIColor.green.withAlphaComponent(0.5)
        case .failure: return UIColor.red.withAlphaComponent(0.5)
        case .warning: return UIColor.yellow.withAlphaComponent(0.6)
        }
    }
    
    func indicator() -> Character {
        switch self {
        case .info: return "ℹ️"
        case .success: return "✅"
        case .failure: return "❌"
        case .warning: return "⚠️"
        }
    }
}
