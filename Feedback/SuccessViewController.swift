//
//  SuccessViewController.swift
//  Feedback
//
//  Created by Mohamed Afsar on 24/02/21.
//

import UIKit
import Gifu

internal final class SuccessViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(_vcTapped)))
        
        let imageVw = GIFImageView()
        imageVw.animate(withGIFNamed: "catShakingHead", animationBlock:  {
            print("It's animating!")
        })
        imageVw.backgroundColor = .clear
        imageVw.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(imageVw)
        NSLayoutConstraint.activate([
            imageVw.leadingAnchor.constraint(equalTo: imageVw.superview!.leadingAnchor),
            imageVw.heightAnchor.constraint(equalTo: imageVw.widthAnchor),
            imageVw.trailingAnchor.constraint(equalTo: imageVw.superview!.trailingAnchor),
            imageVw.bottomAnchor.constraint(equalTo: imageVw.superview!.bottomAnchor),
        ])
    }
}

// MARK: Action functions
private extension SuccessViewController {
    @objc func _vcTapped() {
        self.dismiss(animated: false, completion: nil)
    }
}
